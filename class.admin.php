<?php

if (!defined('ABSPATH')) die;

add_action('wp_enqueue_scripts', 'load_css');

function load_css()
{

    wp_enqueue_style('follow-styles', plugin_dir_url(__FILE__) . 'assets/css/follow-styles.css', array(), '1.1', 'all');
}

add_action('wp_enqueue_scripts', 'load_scripts');

function load_scripts()
{

    wp_register_script('follow-scripts', plugin_dir_url(__FILE__) . 'assets/js/follow-scripts.js', array('jquery'), '1.1', true);
    wp_enqueue_script('follow-scripts');
}

add_action( 'admin_menu', 'stp_api_add_admin_menu' );
add_action( 'admin_init', 'followup_settings_init' );

function stp_api_add_admin_menu(  ) {
    add_options_page( 'Klaviyo Follow UP', 'Klaviyo Follow UP', 'manage_options', 'klaviyo-follow-up', 'stp_api_options_page' );
}

function followup_settings_init(  ) {
    register_setting( 'followUpPlugin', 'followup_settings' );
    add_settings_section(
        'follow_up_section',
        __( 'Settings', 'wordpress' ),
        'followup_settings_section_callback',
        'followUpPlugin'
    );

    add_settings_field(
        'followup_api_key',
        __( 'API Key', 'wordpress' ),
        'api_key_render',
        'followUpPlugin',
        'follow_up_section'
    );

/*     add_settings_field(
        'followup_template_id',
        __( 'Template ID', 'wordpress' ),
        'followup_template_id_render',
        'followUpPlugin',
        'follow_up_section'
    );
 */
    add_settings_field(
        'followup_from_email',
        __( 'From Email', 'wordpress' ),
        'followup_template_from_email_render',
        'followUpPlugin',
        'follow_up_section'
    );

    add_settings_field(
        'followup_subject',
        __( 'Subject', 'wordpress' ),
        'followup_template_subject_render',
        'followUpPlugin',
        'follow_up_section'
    );
}

function api_key_render(  ) {
    $options = get_option( 'followup_settings' );
    ?>
    <input type='text' name='followup_settings[followup_api_key]' value='<?php echo $options['followup_api_key']; ?>'>
    <?php
}

/* function followup_template_id_render(  ) {
    $options = get_option( 'followup_settings' );
    ?>
    <input type='text' name='followup_settings[followup_template_id]' value='<?php echo $options['followup_template_id']; ?>'>
<?php
} */

function followup_template_from_email_render(  ) {
    $options = get_option( 'followup_settings' );
    ?>
    <input type='text' name='followup_settings[followup_from_email]' value='<?php echo $options['followup_from_email']; ?>'>
<?php
}

function followup_template_subject_render(  ) {
    $options = get_option( 'followup_settings' );
    ?>
    <input type='text' name='followup_settings[followup_subject]' value='<?php echo $options['followup_subject']; ?>'>
<?php
}

function followup_settings_section_callback(  ) {
    echo __( 'This Section Description', 'wordpress' );
}

function stp_api_options_page(  ) {
    ?>
    <form action='options.php' method='post'>

        <h2>Follow UP Admin Page</h2>

        <?php
        settings_fields( 'followUpPlugin' );
        do_settings_sections( 'followUpPlugin' );
        submit_button();
        ?>

    </form>
    <?php
}