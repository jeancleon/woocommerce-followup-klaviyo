<?php

function getCampaign($list_id, $campaign_id, $name, $template_id = 'Nb3PDp', $from_email = 'test@klaviyo.com', $from_name = 'John Doe', $subject = 'Testing')
{

    $options = get_option( 'followup_settings' );
    $api_key = $options['followup_api_key'];
    $api_url = null;

    $body = array();
    $method = 'GET';
    $headers = array('Content-type' => 'application/json', 'api-key' => $api_key);

    $api_url = 'https://a.klaviyo.com/api/v1/campaign/' . $campaign_id . '?api_key=' . $api_key;

    $request = new WP_Http();
    $response = $request->request($api_url, array('method' => $method, 'body' => $body, 'headers' => $headers));

    if ($response['response']['code'] == 404) {

        $headers = array('api-key' => $api_key);

        $api_url = 'https://a.klaviyo.com/api/v1/campaigns';
        $method = 'POST';
        $body = array(
            'api_key' => $api_key,
            "list_id" => $list_id,
            "template_id" => $template_id,
            "from_email" => $from_email,
            "from_name" => $from_name,
            "subject" => $subject,
            "name" => $name,
            'use_smart_sending' => false
        );

        $response = $request->request($api_url, array('method' => $method, 'body' => $body, 'headers' => $headers));
    }

    return $response;
}

function sendCampaign($campaign_id)
{

    $options = get_option( 'followup_settings' );
    $api_key = $options['followup_api_key'];
    $api_url = null;

    $body = array();
    $method = 'POST';

    $api_url = 'https://a.klaviyo.com/api/v1/campaign/' . $campaign_id . '/send?api_key=' . $api_key;

    $request = new WP_Http();
    $response = $request->request($api_url, array('method' => $method, 'body' => $body, 'headers' => $headers));

    return $response;
}

function cloneCampaign($campaign_id)
{

    $options = get_option( 'followup_settings' );
    $api_key = $options['followup_api_key'];
    $api_url = null;

    $headers = array('Content-type' => 'application/json', 'api-key' => $api_key);

    $api_url = 'https://a.klaviyo.com/api/v1/campaign/' . $campaign_id . '?api_key=' . $api_key;
    $method = 'GET';

    $request = new WP_Http();
    $response = $request->request($api_url, array('method' => $method, 'body' => $body, 'headers' => $headers));

    if ($response['response']['code'] == 200) {

        $headers = array('api-key' => $api_key);
        $body_campaign = json_decode($response['body'], true);

        $list_id = $body_campaign['lists'][0]['id'];
        $list_name = $body_campaign['lists'][0]['name'];
        $campaign_name = $body_campaign['name'];

        $method = 'POST';

        $api_url = 'https://a.klaviyo.com/api/v1/campaign/' . $campaign_id . '/clone?api_key=' . $api_key;
        $body = array(
            "list_id" => $list_id,
            "name" => $campaign_name,
        );

        $request = new WP_Http();
        $response = $request->request($api_url, array('method' => $method, 'body' => $body, 'headers' => $headers));

    }

    return $response;
}

function getRecipients($campaign_id)
{

    $options = get_option( 'followup_settings' );
    $api_key = $options['followup_api_key'];
    $api_url = null;

    $body = array();
    $method = 'GET';

    $api_url = 'https://a.klaviyo.com/api/v1/campaign/' . $campaign_id . '/recipients?api_key=' . $api_key;

    $request = new WP_Http();
    $response = $request->request($api_url, array('method' => $method, 'body' => $body, 'headers' => $headers));

    return $response;

}
