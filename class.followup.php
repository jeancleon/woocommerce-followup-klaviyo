<?php

include('class.proccess.php');

add_filter('woocommerce_before_add_to_cart_form', 'show_button');

function show_button($variable)
{

    $result = false;

    if (isset($_POST['email']) && isset($_POST['subscribe'])) {

        $email = $_POST['email'];

        $result = subscribe($email, null);

        if ($result) {

            printf('<div class="woocommerce"><div class="woocommerce-message" role="alert">You have subscribed successfully. Please check your email to confirm</div></div>');
        }
    } else if (isset($_POST['email']) && isset($_POST['unsubscribe'])) {

        $email = $_POST['email'];

        $result = unsubscribe($email);
    }

    if (!$result) {

        printf('<div class="follow-up">');

        printf('<a id="follow-up" class="button-follow-up single_add_to_cart_button button alt">%s</a>', esc_html('Follow Puppy'));

        printf('
    <form id="follow-form" name="frm" method="post" action="#">
        <input type="text" value="" name="email" id="email" placeholder="Please insert your Email." required>
        <input type="hidden" value="1" name="subscribe" id="subscribe">
        <input type="submit" value="Submit" name="sub" id="sub">
    </form>');

        printf('</div>');
    } else {

        printf('<div class="follow-up">');

        printf('
    <form name="frm" method="post" action="#">
        <input type="hidden" value="' . $email . '" name="email" id="email">
        <input type="hidden" value="1" name="unsubscribe" id="unsubscribe">
        <input type="submit" value="Unsubscribe" name="sub" id="sub">
    </form>');

        printf('</div>');
    }
}
