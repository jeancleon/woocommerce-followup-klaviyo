<?php

//include_once('class.campaigns.php');

add_action('save_post_product', 'follow_on_product_save', 10, 3);

function follow_on_product_save($post_id, $post)
{

    $options = get_option('followup_settings');
    $product = wc_get_product($post_id);

    $members = 0;

    if ($product->post_type == 'product') {

        if ($product->name != 'AUTO-DRAFT') {

            $result = subscribe(null, $product->get_id());

            if ($result) {

                $list_id = get_field("puppy_klaviyo_list_id", $product->get_id());
                $campaign_id = get_field("puppy_klaviyo_campaign_id", $product->get_id());

                /* if ($list_id && $campaign_id) {

                    $recipients = getRecipients($campaign_id);

                    //$campaign = sendCampaign($campaign_id);
                    $campaign = getCampaign($list_id, $campaign_id, $options['followup_template_id']);

                    print_r($campaign);die;

                    if ($campaign['response']['code'] == 200) {

                        $clone = cloneCampaign($campaign_id);

                        if ($clone['response']['code'] == 200) {

                            $body = json_decode($clone['body'], true);

                            update_field("puppy_klaviyo_campaign_id", $body['id'], $post->ID);
                        }
                    }
                } */

                if ($list_id) {

                    $members = getMemberList($list_id);
                    update_field("puppy_klaviyo_member_count", count($members), $post->ID);

                    if (count($members) > 0) {

                        sendMails($members, $product);
                    }
                }
            }
        }
    }
}


function my_acf_prepare_field($field)
{
    $field['disabled'] = true;

    return $field;
}

add_filter('acf/prepare_field/key=puppy_klaviyo_list_id', 'my_acf_prepare_field');
add_filter('acf/prepare_field/key=puppy_klaviyo_member_count', 'my_acf_prepare_field');


function tipo_de_contenido_html()
{

    return 'text/html';
}

add_filter('wp_mail_content_type', 'tipo_de_contenido_html');


function sendMails($members, $product)
{

    $options = get_option('followup_settings');

    $from = $options['followup_from_email'];
    $subject  = $options['followup_subject'];
    $template = 'The ' . $product->get_name() . ' puppy has been updated. Go to <a href="' . get_permalink($product->get_id()) . '">Visit</a>';

    //$headers = "From:" . $from;

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    $headers .= 'From: ' . $from . "\r\n" .
        'Reply-To: ' . $from . "\r\n" .
        'X-Mailer: PHP/' . phpversion();


    $image = wp_get_attachment_image_src(get_post_thumbnail_id($product->get_id()), 'single-post-thumbnail');

    $message = '<html><body>';
    $message .= '<img src="' . $image[0] . '" alt="' . $product->get_name() . '" />';
    $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
    $message .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>" . $product->get_name() . "</td></tr>";
    $message .= "<tr><td><strong>Price:</strong> </td><td>" . $product->get_price() . "</td></tr>";

    $message .= "<tr><td><strong>Status:</strong> </td><td>" . $product->get_status() . "</td></tr>";
    $message .= "<tr><td><strong>Description:</strong> </td><td>" . $product->get_description() . "</td></tr>";
    $message .= "<tr><td><strong>Go To </td><td><a href=" . get_permalink($product->get_id()) . ">Visit</a></td></tr>";
    
    $message .= "</table>";
    $message .= "</body></html>";

    foreach ($members as $member => $value) {

        mail($value['email'], $subject, $message, $headers);
    }
}

remove_action('after_wcfm_products_manage_meta_save', 'wcfm_customfield_products_manage_meta_save', 10, 2);
add_action( 'after_wcfm_products_manage_meta_save', 'wcfm_customfield_products_manage_meta_save', 200, 2 );


function wcfm_customfield_products_manage_meta_save( $new_product_id, $wcfm_products_manage_form_data ) {
    global $WCFM;
    
    follow_on_product_save($new_product_id, null);
    // Product Custom Fields
    $wcfm_product_custom_fields = get_option( 'wcfm_product_custom_fields', array() );
    if( $wcfm_product_custom_fields && is_array( $wcfm_product_custom_fields ) && !empty( $wcfm_product_custom_fields ) ) {
        foreach( $wcfm_product_custom_fields as $wcfm_product_custom_field ) {
            $is_group = !empty( $wcfm_product_custom_field['group_name'] ) ? 'yes' : 'no';
            $is_group = !empty( $wcfm_product_custom_field['is_group'] ) ? 'yes' : 'no';
            if( $is_group == 'yes' ) {
                $group_name = $wcfm_product_custom_field['group_name'];
                if(isset($wcfm_products_manage_form_data[$group_name]) && !empty($wcfm_products_manage_form_data[$group_name])) {
                    $group_value = $wcfm_products_manage_form_data[$group_name];
                    $group_value = apply_filters( 'wcfm_custom_field_group_data_save', $group_value, $group_name );
                    update_post_meta( $new_product_id, $group_name, $group_value );
                } else {
                    update_post_meta( $new_product_id, $group_name, array() );
                }
            } else {
                $wcfm_product_custom_block_fields = $wcfm_product_custom_field['wcfm_product_custom_block_fields'];
                if( !empty( $wcfm_product_custom_block_fields ) ) {
                    foreach( $wcfm_product_custom_block_fields as $wcfm_product_custom_block_field ) {
                        $field_name = $wcfm_product_custom_block_field['name'];
                        if(isset($wcfm_products_manage_form_data[$field_name])) {
                            update_post_meta( $new_product_id, $field_name, $wcfm_products_manage_form_data[$field_name] );
                        } else {
                            if( $wcfm_product_custom_block_field['type'] == 'checkbox' ) {
                                if( isset($wcfm_products_manage_form_data[$field_name]) ) {
                                    update_post_meta( $new_product_id, $field_name, 'yes' );
                                } else {
                                    update_post_meta( $new_product_id, $field_name, 'no' );
                                }
                            } else {
                                update_post_meta( $new_product_id, $field_name, '' );
                            }
                        }
                    }
                }
            }
        } 
    }
}

