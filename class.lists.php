<?php

function getList($list_id, $list_name = null, $email = null)
{
    $options = get_option( 'followup_settings' );
    $api_key = $options['followup_api_key'];

    $api_url = 'https://a.klaviyo.com/api/v2/list/' . $list_id;
    $body = array();
    $headers = array('Content-type' => 'application/json', 'api-key' => $api_key);
    $method = 'GET';

    $request = new WP_Http();
    $response = $request->request($api_url, array('method' => $method, 'body' => $body, 'headers' => $headers));

    if ($response['response']['code'] == 404) {

        $api_url = 'https://a.klaviyo.com/api/v2/lists';
        $body = json_encode(array("list_name" => $list_name));
        $method = 'POST';

        $response = $request->request($api_url, array('method' => $method, 'body' => $body, 'headers' => $headers));
    }

    return $response;
}

function subscribeUser($email, $list_id)
{

    $options = get_option( 'followup_settings' );
    $api_key = $options['followup_api_key'];

    $api_url = 'https://a.klaviyo.com/api/v2/list/' . $list_id . '/subscribe';
    $headers = array('Content-type' => 'application/json', 'api-key' => $api_key);
    $method = 'POST';

    $body = json_encode(
        array(
            "list_name" => $list_name,
            'profiles' => array(
                'email' => $email
            )
        )
    );

    $request = new WP_Http();
    $response = $request->request($api_url, array('method' => $method, 'body' => $body, 'headers' => $headers));

    if ($response['response']['code'] == 404) {

    }

    return $response;
}

function unsubscribeUser($email, $list_id)
{

    $options = get_option( 'followup_settings' );
    $api_key = $options['followup_api_key'];

    $api_url = 'https://a.klaviyo.com/api/v2/list/' . $list_id . '/subscribe';
    $headers = array('Content-type' => 'application/json', 'api-key' => $api_key);
    $method = 'POST';

    $body = json_encode(
        array(
            "list_name" => $list_name,
            'emails' => [ $email ]
        )
    );

    $request = new WP_Http();
    $response = $request->request($api_url, array('method' => $method, 'body' => $body, 'headers' => $headers));

    if ($response['response']['code'] == 404) {

    }

    return $response;
}

function getMemberList($list_id){

    $options = get_option( 'followup_settings' );
    $api_key = $options['followup_api_key'];

    $api_url = 'https://a.klaviyo.com/api/v2/group/' . $list_id . '/members/all';
    $body = array();
    $headers = array('Content-type' => 'application/json', 'api-key' => $api_key);
    $method = 'GET';

    $request = new WP_Http();
    $response = $request->request($api_url, array('method' => $method, 'body' => $body, 'headers' => $headers));

    if ($response['response']['code'] == 200) {

        $body = json_decode($response['body'], true);

        return $body['records'];

    }

    return null;

}