<?php

include('class.campaigns.php');
include('class.lists.php');

function subscribe($email, $post_id = null)
{

    $options = get_option( 'followup_settings' );
    $template_id = $options['followup_template_id'];    

    global $post;

    if($post == null){
        $post = get_post($post_id);
    }

    $list = null;
    $id = $post->ID;
    $name = $post->post_title;

    $list_name = 'MP - ' . $id . ' - ' . $name;
    $list_id = get_field("puppy_klaviyo_list_id", $post->ID);

    $list = getList($list_id, $list_name);

    if ($list['response']['code'] == 200) {

        $body_list = json_decode($list['body'], true);

        if (!empty($body_list['list_id'])) {

            $list_id = $body_list['list_id'];
        
        }
        
        update_field("puppy_klaviyo_list_id", $list_id, $post->ID);

        if ($email != null) {

            $subscribed = subscribeUser($email, $list_id);

            if ($subscribed['response']['code'] == 200) {

                return true;
            }
        } else {

            return true;
        }

        //return true;

        /* $campaign_id = get_field("puppy_klaviyo_campaign_id", $post->ID);
        $campaign = getCampaign($list_id, $campaign_id, $name, $template_id);

        if ($campaign['response']['code'] == 200) {

            $body_campaign = json_decode($campaign['body'], true);

            $campaign_id = $body_campaign['id'];

            update_field("puppy_klaviyo_campaign_id", $campaign_id, $post->ID);

            if ($email != null) {

                $subscribed = subscribeUser($email, $list_id);

                if ($subscribed['response']['code'] == 200) {

                    return true;
                }
            } else {

                return true;
            }
        } */
    }

    return false;
}

function unsubscribe($email)
{

    global $post;
    $list = null;

    $id = $post->ID;
    $name = $post->post_title;

    $list_id = get_field("puppy_klaviyo_list_id", $post->ID);

    $unsubscribe = unsubscribeUser($email, $list_id);

    if ($unsubscribe['response']['code'] == 200) {

        return true;
    }

    return false;
}
