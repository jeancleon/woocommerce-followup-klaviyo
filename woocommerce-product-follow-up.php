<?php
/*
Plugin Name: Woocommerce Product Follow Up
Plugin URI: https://wordpress.org/plugins/wordpress-importer/
Description: Woocommerce Product Subscription with Klaviyo
Author: wordpressdotorg
Author URI: https://wordpress.org/
Version: 0.6.4
Text Domain: wordpress-importer
License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/

//K88W9e 


if( ! defined( 'ABSPATH' ) ) die;

require_once('class.admin.php');

require_once('class.acf.php');

require_once('class.functions.php');

require_once('class.followup.php');

?>